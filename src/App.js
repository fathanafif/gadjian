import React, {useState, useEffect} from 'react';
import axios from 'axios';

import Personnels from './components/Personnels';
import Navigation from './components/Navigation';
import Pagination from './components/Pagination';

import {Typography, Container, Paper, InputBase, IconButton, Grow, Grid, Button, Card} from '@material-ui/core';
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import SearchIcon from '@material-ui/icons/Search';
import useStyles from './styles';

const App = () => {
  const [personnels, setPersonnels] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [personnelsPerPage] = useState(4);

  //get currentPersonnels
  const indexOfLastPersonnel = currentPage * personnelsPerPage;
  const indexOfFirstPersonnel = indexOfLastPersonnel - personnelsPerPage;
  const currentPersonnels = personnels.slice(indexOfFirstPersonnel, indexOfLastPersonnel);

  const classes = useStyles();
  
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const response = await axios.get('https://randomuser.me/api/?results=28');
      setPersonnels(response.data.results);
      setLoading(false);
    };
    fetchData();
  }, []);

  return (
    <Container className={classes.container} maxwidth="lg">
      
      <Navigation/>

      <Grow in>
          <Grid container>
            <Grid item xs={12} sm={12}>
              <Card className={classes.headerCard}>
                <Grid container>

                  <Grid item sm={7} xs={12} className={classes.leftGrid}>
                    <Typography variant='h5' className={classes.title}>PERSONNEL LIST</Typography>
                    <Typography variant='body2'>List of all personnels</Typography>
                  </Grid>

                  <Grid className={classes.headerCardAction} item sm={5} xs={12}>
                    <Grid container>
                      <Grid item sm={6} xs={12} className={classes.rightGrid}>
                        <Paper component="form" className={classes.searchForm}>
                          <IconButton type="submit" className={classes.searchIcon} aria-label="search">
                            <SearchIcon className={classes.themeColor}/>
                          </IconButton>
                          <InputBase type="text" placeholder="Find Personnels" onChange={event => setSearchTerm(event.target.value)}/>
                        </Paper>
                      </Grid>
                      <Grid item sm={6} xs={12} className={classes.rightGrid}>
                        <Button className={classes.addPersonnelButton} variant="contained" color="primary">
                          ADD PERSONNEL <AddRoundedIcon className={classes.AddIcon}/>
                        </Button>
                      </Grid>
                    </Grid>

                  </Grid>
                </Grid>
              </Card>

              <Personnels personnels={currentPersonnels} loading={loading} searchTerm={searchTerm}/>
              
              <Grid className={classes.pagination} item sm={12}>
                <Pagination
                  currentPage={currentPage}
                  setCurrentPage={setCurrentPage}
                  personnelsPerPage={personnelsPerPage}
                  personnels={personnels}
                />
              </Grid>

            </Grid>
          </Grid>
      </Grow>
    </Container>
  )
};

export default App;