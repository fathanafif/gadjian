import React from 'react';
import moment from 'moment';
import {Grid, Card, CardContent, CardHeader, Divider, Hidden, CircularProgress} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';

import useStyles from '../styles';

const Personnels = ({personnels, loading, searchTerm}) => {
  const classes = useStyles();
  if (loading) {
    return (
      <Grid className={classes.loading}>
        <CircularProgress disableShrink className={classes.themeColor}/>
      </Grid>
    )
  }
  return (
    <Grid container>
      {personnels.filter((pers) => {
        if (searchTerm == null) {
          return pers;
        } else if (pers.name.first.toLowerCase().includes(searchTerm.toLowerCase())) {
          return pers;
        } return 0
      }).map((pers, i) => (
        <Grid item sm={3} xs={12} key={i}>
          <Card className={classes.card}>
            <CardHeader className={classes.cardHeader}
              action={
                <IconButton aria-label="settings" className={classes.moreButton}>
                  <MoreHorizIcon/>
                </IconButton>
              }
              title={
                <div>
                  Personnel ID:
                  <span className={classes.overFlow}>
                    &nbsp;{(pers.id.value == null ? "Not Found" : pers.id.value)}
                  </span>
                </div>
              }
              titleTypographyProps={{variant:'caption'}}
            /><Divider/>
            <CardContent className={classes.cardContent}>
              <Grid container>
                <Grid className={classes.avaGrid} item sm={12} xs={5}>
                  <img className={classes.avatar} src={pers.picture.large} alt="Personnel Avatar" width="86px" height="86px"/>
                </Grid>
                <Grid item sm={12} xs={7}>
                  <div className={classes.row}>
                  <label className={classes.label}>Name</label><br/>
                    {pers.name.first + " " + pers.name.last}
                  </div>
                  <div className={classes.row}>
                    <label className={classes.label}>Telephone</label><br/>
                    {pers.phone}
                  </div>
                  <Hidden xsDown>
                    <div className={classes.row}>
                      <label className={classes.label}>Birthday</label><br/>
                      {moment(pers.dob.date).format("DD-MM")}
                    </div>
                    <Grid className={`${classes.row} ${classes.emailOverflow}`}>
                      <label className={classes.label}>Email</label><br/>
                      {pers.email}
                    </Grid>
                  </Hidden>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      ))}
    </Grid>
  )
}

export default Personnels;