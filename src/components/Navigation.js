import React from 'react';
import {AppBar, Grid, Typography, ListItem, ListItemIcon, ListItemText, Toolbar, Hidden, Avatar, IconButton, Divider} from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home';
import GroupIcon from '@material-ui/icons/Group';
import DateRangeIcon from '@material-ui/icons/DateRange';

import logo from '../images/gadjian.png';

  const drawerWidth = 240;

  const useStyles = makeStyles((theme) => ({
    toolbar: {
        padding: '0px 16px',
    },
    rightNav: {
        float: 'right',
        color: '#616161',
        marginLeft: 'auto',
    },
    greeting: {
        margin: 'auto',
    },
    miniAvatar: {
        marginLeft: '16px',
    },
    themeColor: {
        color: '#23C7C6',
    },
    darkText: {
        color: '#616161',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
          width: drawerWidth,
          flexShrink: 0,
        },
      },
      appBar: {
        [theme.breakpoints.up('sm')]: {
          width: `calc(100% - ${drawerWidth}px)`,
          marginLeft: drawerWidth,
          backgroundColor: '#ffffff',
        },
      },
      menuButton: {
        marginRight: theme.spacing(0),
        [theme.breakpoints.up('sm')]: {
          display: 'none',
        },
      },
      list: {
        padding: '0px',
        [theme.breakpoints.down('xs')]: {
          display: 'none',
        },
      },
      brandDivider: {
        [theme.breakpoints.down('xs')]: {
          display: 'none',
        },
      },
      brand: {
        padding: '10px 16px',
      },
      brandMobile: {
        [theme.breakpoints.up('sm')]: {
          display: 'none',
        },
      },
      // necessary for content to be below app bar
      toolBar: theme.mixins.toolbar,
      toolBarHoriz: {
        backgroundColor: '#ffffff',
        color: '#616161',
        padding: '0px 16px',
      },
      drawerPaper: {
        width: drawerWidth,
        borderRight: '0px',
        color: '#616161',
      },
    }));

  function Navigation(props) {
    const { window } = props;
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);
  
    const handleDrawerToggle = () => {
      setMobileOpen(!mobileOpen);
    };

    const drawer = (
      <div>
        <div className={classes.toolbar}>
          <List className={classes.list}>
            <ListItem className={classes.brand}>
              <a href="/">   
                <img src={logo} alt="gadjian" position="relative" height="40"/>
              </a>
            </ListItem>
          </List>
        </div>
        <Divider className={classes.brandDivider}/>
        <List>
          <ListItem>
            <ListItemIcon><HomeIcon/></ListItemIcon>
            <ListItemText>Beranda</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemIcon><GroupIcon/></ListItemIcon>
            <ListItemText>Personnel List</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemIcon><DateRangeIcon/></ListItemIcon>
            <ListItemText>Attendance</ListItemText>
          </ListItem>
        </List>
      </div>
    );

    const container = window !== undefined ? () => window().document.body : undefined;
      
    return (
      <React.Fragment>
        <AppBar position="fixed" className={classes.appBar}>
            <Toolbar className={classes.toolBarHoriz}>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography noWrap>
              <a href="/">
                <img className={classes.brandMobile} src={logo} alt="gadjian" position="relative" height="40"/>
              </a>
            </Typography>
            <Grid className={classes.rightNav}>
              <Grid container>
                <Hidden xsDown>
                    <span className={classes.greeting}>
                        Hello, &nbsp; 
                        <span className={classes.themeColor}>
                        Gadjian User
                        </span>
                    </span>
                </Hidden>
                <Avatar aria-label="recipe" className={classes.miniAvatar}>
                    A
                </Avatar>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        <nav className={classes.drawer} aria-label="mailbox folders">
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Hidden smUp implementation="css">
            <Drawer
              container={container}
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
              paper: classes.drawerPaper,
              }}
              ModalProps={{
              keepMounted: true, // Better open performance on mobile.
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
                classes={{
                paper: classes.drawerPaper,
                }}
                variant="permanent"
                open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
    </React.Fragment>
  );
};

export default Navigation;