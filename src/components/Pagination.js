import React from 'react';
import {Grid, Button} from '@material-ui/core';

import { ArrowBackIosRounded, ArrowForwardIosRounded } from '@material-ui/icons';
import useStyles from '../styles';

const Pagination = ({
    currentPage, setCurrentPage, personnels, personnelsPerPage
}) => {
    
    const classes = useStyles();
    const pages = [];
    for (let i = 1; i <= Math.ceil(personnels.length / personnelsPerPage); i++) {
        pages.push(i);
    }
    const handleNextBtn = () => {
        setCurrentPage(currentPage + 1);
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
    const handlePrevBtn = () => {
        setCurrentPage(currentPage - 1);
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    return (
        <Grid>
            <Button className={classes.paginationButton} onClick={handlePrevBtn} disabled={currentPage === pages[0] ? true : false}>
                <ArrowBackIosRounded className={classes.buttonIcon}/>
                Prev Page
                </Button>
            <Button className={classes.paginationButton} onClick={handleNextBtn} disabled={currentPage === pages[pages.length - 1] ? true : false}>
                Next Page
                <ArrowForwardIosRounded className={classes.buttonIcon}/>    
            </Button>
        </Grid>
    );

}

export default Pagination;