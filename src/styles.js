import { makeStyles } from '@material-ui/core/styles';

export default makeStyles ( () => ({
    container: {
        padding: '80px 0px 0px 0px',
        display: 'flex',
    },
    themeColor: {
        color: '#23C7C6',
    },
    headerCard: {
        margin: '0px 14px',
        padding: '24px 24px',
        boxShadow: 'none',
        borderRadius: '12px',
    },
    title: {
        // fontSize: '20px',
        fontWeight: 'bold',
        color: '#23C7C6',
    },
    headerCardAction: {
        alignSelf: 'center',
        marginLeft: 'auto',
    },
    leftGrid: {
        padding: '0px 8px',
    },
    rightGrid: {
        padding: '2px 8px',
    },
    searchForm: {
        margin: '4px 0px',
        display: 'flex',
    },
    addPersonnelButton: {
        fontWeight: 'bold',
        backgroundColor: '#23C7C6',
        width: '100%',
        margin: '4px 0px',
        '&:hover': {
            backgroundColor: '#1fb7b5',
        }
    },
    searchIcon: {
        padding: '6px 6px',
    },
    buttonIcon: {
        fontSize: '14px',
        paddingBottom: '0px',
    },
    AddIcon: {
        fontSize: '20px',
        paddingBottom: '2px',
    },
    loading: {
        verticalAlign: 'middle',
        margin: '24px 18px',
        display: 'flex',
        justifyContent: 'center',
    },
    card: {
        margin: '16px',
        borderRadius: '12px',
        fontSize: '14px',
        boxShadow: 'none',
    },
    cardHeader: {
        padding: '8px 16px',
        color: '#616161',
        alignSelf: 'center',
    },
    overFlow: {
        color: '#23C7C6',
        whiteSpace: 'nowrap',
        width: '90px',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        display: 'inline-grid',
        '&:hover': {
            overflow: 'visible',
        },
    },
    emailOverflow: {
        whiteSpace: 'nowrap',
        width: '200px',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        // display: 'inline-grid',
        '&:hover': {
            overflow: 'visible',
        },
    },
    moreButton: {
        margin: '8px 4px 0px 0px',
        padding: '0px 0px 0px 0px',
    },
    cardContent: {
        padding: '12px 16px !important',
        color: '#616161',
    },
    label: {
        fontWeight: 'bold',
        fontSize: '10px',
    },
    row: {
        margin: '4px 0px',
    },
    avatar: {
        borderRadius: '50%',
        display: 'grid',
        margin: 'auto',
    },
    pagination: {
        verticalAlign: 'middle',
        margin: '12px 18px',
        display: 'flex',
        justifyContent: 'center',
        listStyleType: 'none',
    },
    paginationButton: {
        fontWeight: 'bold',
        padding: '2px 18px',
        margin: '0px 8px',
        textTransform: 'capitalize',
        backgroundColor: '#eeeeee',
        color: '#616161',
        '&:hover': {
            backgroundColor: '#1fb7b5',
            // fontWeight: 'bolder',
            color: '#ffffff',
        },
    },
}));